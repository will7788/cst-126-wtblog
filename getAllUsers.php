<?php
/*CST-126 Milestone 5 & 6, William Thornton, Version 1.0, 06/29/2019*/
    session_start();
    require('myFuncs.php');
    $con = dbConnect();

    if(!con)
    {
        echo 'No Connection!';
    }
    
    $sql = 'SELECT id, username, firstname, lastname, user_deleted FROM user_info';
    $result = $con->query($sql);
    
    if ($result->num_rows > 0) {
        // output data of each row
        echo "<h2>Total # of users: ".$result->num_rows."</h2>";
        while($row = $result->fetch_assoc()) {
            if ($row["user_deleted"] == null)
            {
            echo "Name: " . $row["firstname"]. " " . $row["lastname"]. " ID: " 
            . $row["id"] . " Username: " . $row["username"];
            
            if ($_SESSION['userrole'] == 1)
                {
                    echo '<form action="removeUser.php" method="post" name="input" class="">
                        <input type="hidden" name="removebtn" value="'. $row["id"] .'">
                        <input class="btn btn-dark btn-xs" type="submit" 
                        name="submit" value="Remove" style="margin-left:0;">
                        </form>';
                    echo '<form action="changePermissions.php" method="post" name="input">
                        <select class="selectpicker" name="category">
                             <option value="0">0</option>
                             <option value="1">1</option>
                         </select>
                        <input type="hidden" name="removebtn" value="'.$row["id"].'">
                        <input class="btn btn-dark btn-xs" type="submit" 
                        name="submit" value="Change User Role" style=" margin-top: 5px;">
                        </form>';
                }
            echo "<br>";    
            }
        }
    } else {
        echo "0 results";
    }
    
    $con->close();
	
?>