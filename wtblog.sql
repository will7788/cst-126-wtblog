-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:49880
-- Generation Time: Jun 29, 2019 at 02:20 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wtblog`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL,
  `post_title` varchar(255) NOT NULL,
  `post_content` varchar(10000) NOT NULL,
  `posted_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `posted_by` int(11) NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(11) NOT NULL,
  `deleted_flag` varchar(1) NOT NULL DEFAULT 'n',
  `category_id` int(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`post_id`, `post_title`, `post_content`, `posted_date`, `posted_by`, `updated_date`, `updated_by`, `deleted_flag`, `category_id`) VALUES
(2, 'Hi', 'This is william', '2019-06-14 21:07:33', 1, '2019-06-29 21:06:06', 1, 'n', 3),
(14, 'Hi this is William', 'William Thornton', '2019-06-17 02:48:23', 1, '2019-06-23 23:08:17', 1, 'n', 1),
(15, 'Jimmy Boy', 'My name is Jimmy Boy', '2019-06-23 22:15:13', 2, '2019-06-29 21:07:41', 2, 'n', 1),
(16, 'Testing', 'Just testing', '2019-06-26 01:43:17', 5, '2019-06-26 01:43:17', 5, 'n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `PASS_WORD` varchar(255) NOT NULL,
  `birth_day` int(2) NOT NULL,
  `birth_month` int(2) NOT NULL,
  `birth_year` int(4) NOT NULL,
  `user_role` varchar(255) NOT NULL DEFAULT '0',
  `user_deleted` tinyint(1) DEFAULT NULL,
  `user_banned` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `username`, `firstname`, `lastname`, `email`, `PASS_WORD`, `birth_day`, `birth_month`, `birth_year`, `user_role`, `user_deleted`, `user_banned`) VALUES
(1, 'will7788', 'William', 'Thornton', 'rshobbie@gmail.com', 'password2', 25, 1, 1997, '1', NULL, NULL),
(2, 'Jimmy77', 'Jimmy', 'Flag', 'flag@gmail.com', 'password5', 23, 9, 1990, '0', NULL, NULL),
(3, 'jjwatt', 'James', 'Watt', 'watt@gmail.com', 'password5', 13, 11, 1991, '0', NULL, NULL),
(4, 'smithj123', 'Jim', 'Smith', 'name@gmail.com', 'password', 25, 12, 1990, '0', NULL, NULL),
(5, 'tt', 'tt', 'tt', 'ttt@gmail.com', 'ttt', 0, 0, 0, '1', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `posted_by` (`posted_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`posted_by`) REFERENCES `user_info` (`id`),
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user_info` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
