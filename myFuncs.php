<?php
/*CST-126 Milestone 5 & 6, William Thornton, Version 1.0, 06/29/2019*/
session_start();


function dbConnect()
{
    
    $con = mysqli_connect('127.0.0.1:49880', 'azure', '6#vWHD_$', 'wtblog');

    if(!con)
    {
        echo 'No Connection!';
        exit;
    }

    if(!mysqli_select_db($con, 'wtblog'))
    {
        echo 'Database not selected';
        exit;
    }
    return $con;
}

function saveUserId($id)
{
    $_SESSION["USER_ID"] = $id;
}

function getUserId()
{
	return $_SESSION["USER_ID"];
}

function contentFilter($text)
{
    $termArray = array('\bass(es|holes?)?\b', '\bshit(e|ted|ting|ty|head)?\b', '\bfag(g|got)?\b', '\bfuck(k|er|ing|ed|head)?\b');
    $filtered_text = $text;
    foreach($termArray as $word)
    {
        $match_count = preg_match_all('/' . $word . '/i', $text, $matches);
        for($i = 0; $i < $match_count; $i++)
            {
                $newstr = trim($matches[0][$i]);
                $filtered_text = preg_replace('/\b' . $newstr . '\b/', str_repeat("*", strlen($newstr)), $filtered_text);
            }
    }
    return $filtered_text;
}


?>