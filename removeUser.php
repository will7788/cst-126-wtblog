<?php
/*CST-126 Milestone 5 & 6, William Thornton, Version 1.0, 06/29/2019*/
    session_start();
    require('myFuncs.php');
    $con = dbConnect();

    $userid = $_POST[removebtn];
    
    $sql = "UPDATE user_info SET user_deleted = '1' WHERE id = ?;";
    $stmt = $con->prepare($sql);
    $stmt->bind_param('s', $userid);
    $stmt->execute();
    $con->close();
    header("refresh:1; url=users.php")

?>